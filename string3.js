// ==== String Problem #3 ====
// Given a string in the format of "10/01/2021", print the month in which the date is present in.


function string3(file, monthArray) {
    let dateString = new Date(file)
    let findMonth = dateString.getMonth()
    return monthArray[findMonth];
}



module.exports = string3