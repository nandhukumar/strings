// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and return it in an array in numeric values.
// [111, 139, 161, 143]. Support IPV4 addresses only. If there are other characters detected, return an empty array.


function string2(file) {
    array = file.split(".")
    finalArray = []
    for (i = 0; i < array.length; i++) {
        string = array[i]
        if (!(isNaN(string))) {
            finalArray.push(parseInt(string))
        }
        else {
            return [];
        }
    }
    return finalArray;
}



module.exports = string2